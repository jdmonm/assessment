# Import basic Framework tools.
from flask import Flask, request, jsonify
# Import cors library
from flask_cors import CORS

# Import Loan Class
from modules.loan.LoanClass import Loan as LoanClass


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/api/')
def hello_world():
    return jsonify('Hello, World!')

# Declare create-loan api path
# Method: post
@app.route('/api/create-loan', methods=['POST'])
# Declare createLoan function
def createLoan():
    # Instance of LoanClass
    lc = LoanClass()
    # Return response
    return jsonify(lc.evaluateLoan(request.get_json()))