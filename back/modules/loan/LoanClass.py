# Declare Loan class
class Loan:

    # Declare class method evaluateLoan
    # Input: object data
    # Output: object with input and loan status
    @classmethod
    def evaluateLoan(cls, data):
        # Declare Variables
        status = ''
        requested_amount = int(data.get('requested_amount')) 

        # Validate loan condition
        if requested_amount > 50000:
            status = 'Declined'
        elif requested_amount == 50000:
            status = 'Undecided'
        else:
            status ='Approved'

        # Return obj
        return {
            'status': status,
            'business_name': data.get('business_name'),
            'requested_amount': data.get('requested_amount'),
            'tax_id': data.get('requested_amount')
        }